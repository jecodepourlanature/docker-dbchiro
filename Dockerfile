FROM debian:stretch

RUN apt-get update && apt-get install -y git python3 python3-pip virtualenv \
	python3-virtualenv apache2 libapache2-mod-wsgi-py3 libgdal20
RUN git clone --depth=1 https://framagit.org/dbchiro/dbchiroweb.git /var/www/dbchiro
WORKDIR /var/www/dbchiro
ADD install.sh /tmp/install.sh
RUN bash /tmp/install.sh
ADD config.py dbchiro/settings/configuration/config.py
CMD sleep 86400

